﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Npgsql;



namespace GetUsage
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //PostgreSQL Connection String
        //string dsn = "Server=db1;Port=9822;Database=healthcare;Uid=statuser;Pwd=aig73rc%s;ssl mode=Prefer;Trust Server Certificate=true";

        //SQL Server
        string connString = ConfigurationManager.ConnectionStrings["Web1"].ConnectionString;


        protected void Page_Load(object sender, EventArgs e)
        {
            //PostgreSQL
            //NpgsqlConnection sqlCon = new NpgsqlConnection(dsn);
            //sqlCon.Open();

            string comStr = "Select * from v_GetLeads";


            //SQL Server
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(comStr, conn);
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            //PostgreSQL
            //NpgsqlCommand com = new NpgsqlCommand(comStr, sqlCon);

            //PostgreSQL
            //NpgsqlDataAdapter ad = new NpgsqlDataAdapter(com);


            DataTable dt = new DataTable();


            //PostgreSQL
            //
            //ad.Fill(dt);

            //SQLServer
            // this will query your database and return the result to your datatable
            da.Fill(dt);

            gvLeads.DataSource = dt;
            gvLeads.DataBind();

            conn.Close();
            da.Dispose();

        }
    }
}