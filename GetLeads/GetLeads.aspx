﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetLeads.aspx.cs" Inherits="GetUsage.WebForm1" %>

<!DOCTYPE html>

 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvLeads" runat="server" AutoGenerateColumns ="false">
        <Columns >
            <asp:BoundField DataField ="prospect_name" HeaderText ="Prospect Name" />
            <asp:BoundField DataField ="prospect_cellphone" HeaderText ="Prospect Cellphone" />
            <asp:BoundField DataField ="prospect_homephone" HeaderText ="Prospect Homephone" />
            <asp:BoundField DataField ="prospect_workphone" HeaderText ="Prospect Workphone" />
            <asp:BoundField DataField ="prospect_email" HeaderText ="Prospect Email" />
            <asp:BoundField DataField ="state" HeaderText ="State" />
            <asp:BoundField DataField ="created_on" HeaderText ="Created Date" />
            <asp:BoundField DataField ="status" HeaderText ="Status" />
            <asp:BoundField DataField ="source" HeaderText ="Source" />
            <asp:BoundField DataField ="campaign" HeaderText ="Campaign" />
            <asp:BoundField DataField ="prodtype" HeaderText ="Prodtype" />
            <asp:BoundField DataField ="product" HeaderText ="Product" />
            <asp:BoundField DataField ="agentname" HeaderText ="Agent First Name" />
            <asp:BoundField DataField ="agentname" HeaderText ="Agent Last Name" />
            <asp:BoundField DataField ="agency" HeaderText ="Agency" />
        </Columns>

    </asp:GridView>
    </div>
    </form>
</body>
</html>
