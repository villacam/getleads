﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace GetUsage
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string connectionString = @"Data Source=ats-sql; Initial Catalog = ACA; Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {
            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDA = new SqlDataAdapter("Select * from Agent", sqlCon);
                DataTable dtbl = new DataTable();

                sqlDA.Fill(dtbl);
                gvUsage.DataSource = dtbl;
                gvUsage.DataBind();
            }

        }
    }
}