﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="GetUsage.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvUsage" runat="server" AutoGenerateColumns ="false">
        <Columns >
            <asp:BoundField DataField ="AGNAME" HeaderText ="Agent Name" />
            <asp:BoundField DataField ="AGADDR" HeaderText ="Address" />
            <asp:BoundField DataField ="AGCITY" HeaderText ="City" />
            <asp:BoundField DataField ="AGSTATE" HeaderText ="State" />
            <asp:BoundField DataField ="AGZIP" HeaderText ="ZIP" />
        </Columns>

    </asp:GridView>
    </div>
    </form>
</body>
</html>
